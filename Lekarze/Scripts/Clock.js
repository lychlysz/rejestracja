﻿function myClock() {
    var d = new Date();
    var hours = d.getHours();
    var mins = d.getMinutes();
    var sec = d.getSeconds();
    if (hours >= 0 && hours < 10)
    {
        hours = "0" + hours;
    }
    if (mins >= 0 && mins < 10)
    {
        mins = "0" + mins;
    }
    if (sec >= 0 && sec < 10)
    {
        sec = "0" + sec;
    }
    var el = document.getElementById("clock")
    el.innerHTML = hours + ":" + mins + ":" + sec; 
    }setInterval(myClock, 1000)