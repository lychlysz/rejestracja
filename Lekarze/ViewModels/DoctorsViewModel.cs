﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lekarze.Models;

namespace Lekarze.ViewModels
{
    public class DoctorsViewModel
    {
        public Doctor Doctor { get; set; }
        public List<Doctor> List { get; set; }
        public IEnumerable<Genre> Genre { get; set; }
    }
}