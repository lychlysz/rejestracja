﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Lekarze.Models;

namespace Lekarze.ViewModels
{
    public class ConfirmViewModel
    {
        public Visit Visit { get; set; }
        public Doctor Doctor { get; set; }
        public Person Person { get; set; }
    }
}