namespace Lekarze.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addperson2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.People",
                c => new
                {
                    PersonId = c.Int(nullable: false, identity: true),
                    Name = c.String(),
                    LastName = c.String(),
                })
                .PrimaryKey(t => t.PersonId);
            AlterColumn("dbo.People", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.People", "LastName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.People", "LastName", c => c.String());
            AlterColumn("dbo.People", "Name", c => c.String());
        }
    }
}
