﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Lekarze.Models;

namespace Lekarze.DAL
{
    public class ClinicContext : DbContext
    {
        public ClinicContext() : base("ClinicDatabase")
        {

        }

        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Genre> Genre { get; set; }
        public DbSet<Visit> Visits { get; set; }
        public DbSet<Person> Persons { get; set; }
    }
}