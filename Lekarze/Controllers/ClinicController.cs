﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Lekarze.DAL;
using Lekarze.Models;
using Lekarze.ViewModels;

namespace Lekarze.Controllers
{
    public class ClinicController : Controller
    {
        private ClinicContext db = new ClinicContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Register(Doctor doctor)
        {           
            var vm = new DoctorsViewModel()
            {
                Doctor = doctor,
                Genre = db.Genre.ToList()
            };

            return View(vm);
        }

        [ChildActionOnly]
        public ActionResult Register2(int id)
        {
            var list = db.Doctors.ToList();

            if (id !=0)
                list = db.Doctors.Where(n=>n.GenreId == id).ToList();

            var vm = new DoctorsViewModel()
            {
                List = list,
                Genre = db.Genre.ToList()
            };
            
            return PartialView("_ListaLekarzy", vm);
        }

        public ActionResult RegisterDay(int id)
        {
            var doktor = db.Doctors.SingleOrDefault(d=> d.DoctorID == id);

            if (doktor == null)
                return HttpNotFound(); 

            ViewBag.listaDni = db.Visits.ToList().Where(n => n.DoctorId == id).Select(n => n.Day).Distinct();

            return View(doktor);
        }

        [ChildActionOnly]
        public ActionResult Termins(string day, int id)
        {
            var listaGodzin = db.Visits.ToList()
                .Where(n => n.DoctorId == id && n.Day.Equals(day) && n.PersonId == null)
                .Select(n => n);
            return PartialView("_ListaPrzyjec", listaGodzin);

        }

        public ActionResult Confirm(int id)
        {
            var wizyta = db.Visits.SingleOrDefault(w=> w.VisitId == id);
            if (wizyta == null)
                return HttpNotFound();
            var vm = new ConfirmViewModel()
            {
                Visit = wizyta,
                Doctor = db.Doctors.SingleOrDefault(d=>d.DoctorID == wizyta.DoctorId)
            };
           
            return View(vm);
        }

        [HttpPost]
        public ActionResult Confirmed(ConfirmViewModel confirm)
        {
            if (!ModelState.IsValid)
            {
                var vm = new ConfirmViewModel()
                {
                    Visit= confirm.Visit,
                    Doctor = db.Doctors.SingleOrDefault(d => d.DoctorID == confirm.Visit.DoctorId)
                };
                return View("Confirm", vm);
            }

            db.Persons.Add(confirm.Person);
            var visitInDb = db.Visits.SingleOrDefault(v => v.VisitId == confirm.Visit.VisitId);
            if (visitInDb == null)
                return HttpNotFound();
            visitInDb.PersonId = confirm.Person.PersonId;
            db.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}