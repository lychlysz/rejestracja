﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lekarze.Models
{
    public class Doctor
    {
        public int DoctorID { get; set; }
        public string DoctorName { get; set; }
        public string DoctorLastName { get; set; }
        public int GenreId { get; set; }

        public virtual Genre Genre { get; set; }
        public virtual ICollection<Visit> Visit { get; set; }
    }
}