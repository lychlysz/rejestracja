﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lekarze.Models
{
    public class Genre
    {
        public int GenreId { get; set; }
        public string GenreName { get; set; }

        public virtual ICollection<Doctor> Doctors { get; set; }
    }
}