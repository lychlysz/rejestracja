﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Lekarze.Models
{
    public class Visit
    {
        public int VisitId { get; set; }
        public string Day { get; set; }
        public DateTime Time { get; set; }
        public int? PersonId { get; set; }
        public int DoctorId { get; set; }

        public virtual Person Person { get; set; }
        public virtual Doctor Doctor { get; set; }        
    }
}