﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Lekarze.Models
{
    public class Person
    {
        public int PersonId { get; set; }

        [Required(ErrorMessage = "*Pole Wymagane")]
        [Display(Name = "Imię")]
        public string Name { get; set; }

        [Required(ErrorMessage = "*Pole Wymagane")]
        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }

        public virtual ICollection<Visit> Visit { get; set; }
    }
}